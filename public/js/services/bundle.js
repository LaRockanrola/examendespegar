
angular.module('angularExamen')
    .factory('highlightHotels', function($http) {
        return {
            get : function() {
                return $http.get('/api/hotels/highlights');
            }
        }
    })
    .factory('hotels', function($http) {
        return {
            get : function() {
                return $http.get('/api/hotels');
            }
        }
    });

