var app = angular.module('angularExamen', ['ui.router']);

app.config(function($stateProvider, $urlRouterProvider) {

	$urlRouterProvider.otherwise('/');

    $stateProvider
    	.state('home', {
            url: '/',
            templateUrl: 'views/main.html'
        })
});

