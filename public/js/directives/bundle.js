angular.module('angularExamen')
	.directive('asidecomponents', function() {
	    return {
	    	restrict: 'E',
	      	templateUrl: 'js/directives/asideComponents.html'
	    };
  	})
	.directive('breadcrum', function() {
	    return {
	    	restrict: 'E',
	      	templateUrl: 'js/directives/breadcrum.html'
	    };
  	})
  	.directive('countryhotels', function() {
	    return {
	    	restrict: 'E',
	      	templateUrl: 'js/directives/countryHotels.html'
	    };
  	})
  	.directive('discoverdestinations', function() {
	    return {
	    	restrict: 'E',
	      	templateUrl: 'js/directives/discoverDestinations.html'
	    };
  	})
  	.directive('hotelitem', function() {
	    return {
	    	restrict: 'E',
	      	templateUrl: 'js/directives/hotelItem.html'
	    };
  	})
  	.directive('hotellist', function() {
	    return {
	    	restrict: 'E',
	      	templateUrl: 'js/directives/hotelList.html'
	    };
  	})
  	.directive('mainform', function() {
	    return {
	    	restrict: 'E',
	      	templateUrl: 'js/directives/mainForm.html'
	    };
  	})
  	.directive('otherdestinations', function() {
	    return {
	    	restrict: 'E',
	      	templateUrl: 'js/directives/otherDestinations.html'
	    };
  	})
  	.directive('searchtitle', function() {
	   	return {
	    	restrict: 'E',
	      	templateUrl: 'js/directives/searchTitle.html'
	    };
  	})
  	.directive('hotelavailability', function() {
	   	return {
	    	restrict: 'E',
	      	templateUrl: 'js/directives/hotelAvailability.html',
	      	link : function(scope, element, attrs){
	      		// Inicializo el datetime picker
	      		$('.customDateTimePicker').datetimepicker({
		            language: 'es',
		            minView: 2,
		            autoclose : true
		        });

		        // Inicializo el botón de buscar
		        $('#searchAvailability').on('click', function () {
				    var $btn = $(this).button('loading');
  				})

		        // Inicializo el botón de reservar
		        $('.closeAvailability').on('click', function () {
					$('#searchAvailability').button('reset')
					$('.customDateTimePicker')[0].value = "";
					$('.customDateTimePicker')[1].value = "";
  				})

	      		scope.showHotelAvailability = function(availability){
	      			$('.customDateTimePicker').datetimepicker('setStartDate', availability.from);
		        	$('.customDateTimePicker').datetimepicker('setEndDate', availability.to);
		        	$('.customDateTimePicker')[0].value = availability.from;
					$('.customDateTimePicker')[1].value = availability.to;
					
	      			$('.hotelAvailability').modal('show')
	      		}
	      	}

	    };
  	})