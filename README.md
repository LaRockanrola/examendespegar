## Requisitos
- NodeJs

## Instalación
Desde línea de comandos ejecutar en el directorio raíz de la aplicación "npm install"
Luego de una instalación exitosa, ejecutar el comando "node server.js"
Ingresar al navegador a la dirección "http://localhost:8080"

## Aplicación en la nube
http://despegarexamen.herokuapp.com/
