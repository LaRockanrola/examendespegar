
angular.module('angularExamen')
	.controller('mainController', function($scope, highlightHotels, hotels) {

		highlightHotels.get().success(function(data) {
			$scope.highlightHotels = data.highlightHotels;
        });

		hotels.get().success(function(data) {
			$scope.hotels = data.hotels;
        });
	});

