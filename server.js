// server.js

// Configuración inicial
// =============================================================================
var express    = require('express');
var app        = express();

app.use(express.static(__dirname + '/public'));

// Ruteo
// =============================================================================

var router = express.Router();

// Seteo puerto
var port = process.env.PORT || 8080; 		

// Middleware personalizado
router.use(function(req, res, next) {
	console.log('Recibiendo request.');
	next(); 
});

router.get('/', function(req, res) {
	res.sendFile(__dirname + '/public/index.html');
});

app.use('/api', router);

// sample api route
app.get('/api/hotels', function(req, res) {
	res.json({	
	"hotels" :
		[ 
			{
				"name" : "Rio Othon Palace",
				"address" : "Cesare Street 321",
				"stars" : "3",
				"description" : "Confortable",
				"rating": "8",
				"comments" : "20",
				"price" : "215",
				"availability" : {
					"from" : "2013-03-02",
					"to" : "2013-03-22"
				},
				"image":"//www.colombia.travel/es/components/com_mtree/img/listings/s/36.jpg"
			},
			{
				"name" : "Granada Hotel Rio de Janeiro",
				"address" : "Fake street 123",
				"stars" : "4",
				"description" : "Muy Confortable",
				"rating": "8",
				"comments" : "60",
				"price" : "148",
				"availability" : {
					"from" : "2013-02-01",
					"to" : "2013-02-28"
				},
				"image":"//logos.mexicohoteles.com.mx/foto-Hotel-Emporio-Ixtapa-1359225093.jpg"
			},
			{
				"name" : "Ibiza Hotel",
				"address" : "Via Cesare Battisti 133",
				"stars" : "2",
				"description" : "Poco confortable",
				"rating": "5",
				"comments" : "10",
				"price" : "127",
				"availability" : {
					"from" : "2013-05-12",
					"to" : "2013-05-18"
				},
				"image":"//www.colombia.travel/es/components/com_mtree/img/listings/s/151.jpg"
			},
			{
				"name" : "Rio Othon Palace 2",
				"address" : "Cesare Street 221",
				"stars" : "3",
				"description" : "Confortable",
				"rating": "7",
				"comments" : "5",
				"price" : "200",
				"availability" : {
					"from" : "2013-03-06",
					"to" : "2013-03-10"
				},
				"image":"//www.colombia.travel/es/components/com_mtree/img/listings/s/118.jpg"
			}
		]
	});
});

// sample api route
app.get('/api/hotels/highlights', function(req, res) {
	res.json({	
	"hotels" :
		[ 
			{
				"countryDestinations" : {
					"description" : "Hoteles en México",
					"locations" : ["Hoteles en México", "Hoteles en Ciudad de México", "Hostel en Río de Janeiro",
					 "Cabaña en Río de Janeiro","Hoteles en Puerto Vallarta","Hoteles en Acapulco",
					 "Hoteles en Chihuahua","Hoteles en Cuernavaca","Hoteles en Chihuahua","Hoteles en Cuernavaca"]
				}
			},
			{
				"bestDestinations" : {
					"description" : "Hoteles en los mejores destinos",
					"locations" : ["Hoteles en París", "Hoteles en Roma", "Hoteles en Nueva York",
					 "Hoteles en Praga","Hoteles en Veneziar","Hoteles en Washington",
					 "Hoteles en Dubai","Hoteles en Orlando","Hoteles en Bangkok","Hoteles en Barcelona",
					 "Hoteles en Estambul","Hoteles en Shanghai","Hoteles en Berlín","Destinos a descubrir"]
				}
			},
			{
				"findDestinations" : {
					"description" : "Destinos a descubrir",
					"locations" : ["Hoteles en Curacao, Holanda", "Hoteles en Aeropuerto Interna", 
					 "Hoteles en Cavalcante, Brasil", "Hoteles en Courcheval, Francia","Hoteles en Covington, USA",
					 "Hoteles en Caviahue, Argentina", "Hoteles en Convington, USA","Hoteles en Convington, USA",
					 "Hoteles en Convington, USA", "Hoteles en Convington, USA"]
				}
			}

		]
	});
});

// Inicio del server
// =============================================================================
app.listen(port);
console.log('Escuchando el puerto ' + port);